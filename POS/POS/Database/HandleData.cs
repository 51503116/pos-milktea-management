﻿using POS.Model;
using POS.Utils;
using System;
using System.Linq;
namespace POS.Database
{
    class HandleData
    {
        MilkteaLinqToSQLDataContext dc;
        public HandleData()
        {
            dc = new MilkteaLinqToSQLDataContext(UtilComponent.CONNECTTION_STRING);
        }

        #region Account Handle Data
        /// <summary>
        /// Change password of logged user
        /// </summary>
        public bool ChangePassword(string newPwd)
        {
            var accountList = (from account in dc.Accounts
                               where account.UserName == Utils.UtilComponent.user.UserName1
                               select account
                               ).ToList();
            if (accountList.Count() > 0)
            {
                foreach (var account in accountList)
                {
                    account.PassWord = newPwd;
                }
                dc.SubmitChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Create new Account
        /// </summary>
        public bool CreateAccount(string userName, string displayName, string pwd, int isAdmin)
        {
            var accountList = (from account in dc.Accounts
                               where account.UserName == userName
                               select account
                               ).ToList();
            if (accountList.Count() == 0)
            {
                Account account = new Account();
                account.UserName = userName;
                account.DisplayName = displayName;
                account.PassWord = pwd;
                account.isAdmin = isAdmin;
                account.isAvaiable = 1;
                account.Phone = "";
                dc.Accounts.InsertOnSubmit(account);
                dc.SubmitChanges();
                return true;
            }
            return false;
        }
        #endregion

        #region Billing Handle Data
        public void InsertBill(Bill dbBill)
        {
            dc.Bills.InsertOnSubmit(dbBill);
            dc.SubmitChanges();
        }

        /// <summary>
        /// Update Item Price
        /// </summary>
        public void ModifyItem(Database.ItemDetail itemDetail, string size , string price)
        {
            bool isExist = CheckExistItem(itemDetail.id);

            // Update both table
            if (isExist == true)
            {
                // Update Item Price 
                var itemPriceList = (from tbItemPrice in dc.ItemPrices
                                     where tbItemPrice.id_itemprice == itemDetail.id && tbItemPrice.size == size
                                     orderby tbItemPrice.id_itemprice
                                     select tbItemPrice).ToList();
                // Create new size item in itemPrice
                if (itemPriceList.Count() == 0)
                {
                    Database.ItemPrice itemPrice = new Database.ItemPrice();
                    itemPrice.id_itemprice = itemDetail.id;
                    itemPrice.size = size;
                    itemPrice.isAvaiable = 1;
                    itemPrice.Price = price != "" ? Int32.Parse(price) : 0;
                    dc.ItemPrices.InsertOnSubmit(itemPrice);
                }
                // Update existed one
                else
                {
                    foreach (var itemPrice in itemPriceList)
                    {
                        itemPrice.id_itemprice = itemDetail.id;
                        itemPrice.size = size;
                        itemPrice.Price = Int32.Parse(price);
                    }
                }
                // Update Item Detail
                var itemDetailList = (from tbItemDetail in dc.ItemDetails
                                      where tbItemDetail.id == itemDetail.id && tbItemDetail.isAvaiable == 1
                                      select tbItemDetail).ToList();
                foreach (Database.ItemDetail item in itemDetailList)
                {
                    item.id = itemDetail.id;
                    item.name = itemDetail.name;
                    item.isAvaiable = 1;
                    item.idCategory = itemDetail.idCategory;
                }
            }
            // Insert both
            else
            {
                // Insert Item Detail
                itemDetail.isAvaiable = 1;
                dc.ItemDetails.InsertOnSubmit(itemDetail);
                // Insert Item Price
                Database.ItemPrice itemPrice = new Database.ItemPrice();
                itemPrice.id_itemprice = itemDetail.id;
                itemPrice.size = size;
                itemPrice.isAvaiable = 1;
                itemPrice.Price = price != "" ?  Int32.Parse(price) : 0;
                dc.ItemPrices.InsertOnSubmit(itemPrice);
            }          
            dc.SubmitChanges();
        }
        
        /// <summary>
        /// Delete item
        /// </summary>
        public void DeleteItem(Item itemDetail)
        {
            bool isExist = CheckExistItem(itemDetail.Id);
            if (isExist == true)
            {
                var itemPriceList = (from tbItemPrice in dc.ItemPrices
                                     where tbItemPrice.id_itemprice == itemDetail.Id && tbItemPrice.size == itemDetail.Size
                                     select tbItemPrice).ToList();
                // Inactive item
                if (itemPriceList.Count() > 0)
                {
                    foreach (var itemPrice in itemPriceList)
                    {
                        itemPrice.isAvaiable = 0;
                    }
                    dc.SubmitChanges();
                }
            }
        }


        private bool CheckExistItem(string itemID)
        {
            var drinkList = (from itemDetail in dc.ItemDetails
                             where itemDetail.id == itemID
                             select new
                             {
                                 itemDetail.id,
                             }).ToList();
            int count = drinkList.Count();
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        #endregion
    }
}
