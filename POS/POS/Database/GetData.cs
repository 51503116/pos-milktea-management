﻿using System.Collections.Generic;
using System.Linq;
using POS.Model;
using System.Collections.ObjectModel;
using System;
using POS.Utils;

namespace POS.Database
{
    class GetData
    {
        MilkteaLinqToSQLDataContext dc;
        List<object> itemlist;
        ObservableCollection<Item> itemList2;
        ObservableCollection<Category> catList;
        ObservableCollection<Bill> billingList;

        public GetData()
        {
            dc = new MilkteaLinqToSQLDataContext(UtilComponent.CONNECTTION_STRING);
            itemlist = new List<object>();
            itemList2 = new ObservableCollection<Item>();
            catList = new ObservableCollection<Category>();
            billingList = new ObservableCollection<Bill>();
        }

        /// <summary>
        /// Get item by category id
        /// </summary>
        public List<object> GetItemMenu(int categoryId)
        {
            itemlist.Clear();
             var drinkList = (from itemDetail in dc.ItemDetails
                     join itemPrice in dc.ItemPrices on itemDetail.id equals itemPrice.id_itemprice
                     where itemDetail.idCategory == categoryId && itemDetail.isAvaiable == 1 && itemPrice.isAvaiable == 1
                     orderby itemPrice.id_itemprice
                     select new
                     {
                          itemDetail.id,
                          itemDetail.name,
                          itemPrice.size,
                          itemPrice.Price
                     }).ToList();
            foreach( var menuItem in drinkList)
            {
                Item item = new Item(menuItem.id, menuItem.name, menuItem.size, categoryId, (int) menuItem.Price);
                itemlist.Add(item);
            }
            return itemlist;
        }

        /// <summary>
        /// Get List by category ID
        /// </summary>
        public ObservableCollection<Item> GetItemMenu2(int categoryId)
        {
            itemList2.Clear();
            var drinkList = (from itemDetail in dc.ItemDetails
                             join itemPrice in dc.ItemPrices on itemDetail.id equals itemPrice.id_itemprice
                             where itemDetail.idCategory == categoryId && itemDetail.isAvaiable == 1 && itemPrice.isAvaiable == 1
                             orderby itemPrice.id_itemprice
                             select new
                             {
                                 itemDetail.id,
                                 itemDetail.name,
                                 itemPrice.size,
                                 itemPrice.Price
                             }).ToList();
            foreach (var menuItem in drinkList)
            {
                Item item = new Item(menuItem.id, menuItem.name, menuItem.size, categoryId, (int)menuItem.Price);
                itemList2.Add(item);
            }
            return itemList2;
        }

        /// <summary>
        /// Get All Category into List
        /// </summary>
        public ObservableCollection<Category> GetCategoryList()
        {
            catList.Clear();
            var catQueryList = (from itemCategory in dc.ItemCategories
                                where itemCategory.isAvaiable == 1
                                orderby itemCategory.id
                                select new
                                {
                                    itemCategory.id,
                                    itemCategory.name,
                             }).ToList();
            foreach ( var catItem in catQueryList)
            {
                Category cat = new Category(catItem.id.ToString(), catItem.name);
                catList.Add(cat);
            }
            return catList;
        }

        /// <summary>
        /// Get Billing List by filter 
        /// </summary>
        public ObservableCollection<Bill> GetBillingListByDate(DateTime date)
        {
            billingList.Clear();
            var billingDbList = (from bill in dc.Bills
                                 where bill.DateTime.Date == date && bill.isAvaiable == 1
                                 orderby bill.id_Bill
                             select new
                             {
                                 bill.id_Bill,
                                 bill.idAccount,
                                 bill.DateTime,
                                 bill.totalPrice
                             }).ToList();
            foreach (var billItem in billingDbList)
            {
                Bill bill = new Bill();
                bill.id_Bill = billItem.id_Bill  ;
                bill.idAccount = billItem.idAccount;
                bill.DateTime = billItem.DateTime;
                bill.totalPrice = Convert.ToInt32(billItem.totalPrice);
                billingList.Add(bill);
            }
            return billingList;
        }

        /// <summary>
        /// Get Billing List by filter 
        /// </summary>
        public ObservableCollection<Bill> GetBillingListByMonth(int month)
        {
            billingList.Clear();
            var billingDbList = (from bill in dc.Bills
                                 where bill.DateTime.Month == month && bill.isAvaiable == 1
                                 orderby bill.id_Bill
                                 select new
                                 {
                                     bill.id_Bill,
                                     bill.idAccount,
                                     bill.DateTime,
                                     bill.totalPrice
                                 }).ToList();
            foreach (var billItem in billingDbList)
            {
                Bill bill = new Bill();
                bill.id_Bill = billItem.id_Bill;
                bill.idAccount = billItem.idAccount;
                bill.DateTime = billItem.DateTime;
                bill.totalPrice = Convert.ToInt32(billItem.totalPrice);
                billingList.Add(bill);
            }
            return billingList;
        }

        /// <summary>
        /// Check Login with encryption password
        /// </summary>
        public bool CheckLogin(string userName, string passWord)
        {
            string enCodePassword = Base64Encode(passWord);
            var accountList = (from account in dc.Accounts
                           where account.UserName == userName && account.PassWord == enCodePassword && account.isAvaiable == 1
                           select new
                           {
                               account.UserName,
                               account.DisplayName,
                               account.isAdmin,
                           }).ToList();
            if(accountList.Count > 0 )
            {          
                foreach( var account in accountList)
                {
                    UtilComponent.user = new User(account.UserName, account.DisplayName, account.isAdmin);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check current pwd of logged user
        /// </summary>
        public bool CheckCurrentPwd(string currPwd)
        {
            var accountList = (from account in dc.Accounts
                               where account.UserName == Utils.UtilComponent.user.UserName1
                               select new
                               {
                                   account.PassWord,
                               }).ToList();
            if (accountList.Count > 0)
            {
                foreach (var account in accountList)
                {
                    if (account.PassWord.Equals(currPwd))
                        return true;
                }
                return false;
            }
            return false;
        }
        
        /// <summary>
        /// Get Bill filter by Month and Year
        /// </summary>
        public double GetBill_Month_Year(int month, string year)
        {
            double? price = (from bill in dc.Bills
                                 where bill.DateTime.Month == month && bill.DateTime.Year == Int32.Parse(year) && bill.isAvaiable == 1
                                 select bill.totalPrice
                                 ).Sum();
            if (price != null)
            {
                double sum = (double)price;
                return sum;
            }        
            return 0.0;
        }


        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
