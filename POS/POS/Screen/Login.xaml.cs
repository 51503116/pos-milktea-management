﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using POS.Database;
using POS.Utils;
using POS.Screen.Dialog;

namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        GetData dataCollection;
        public Login()
        {
            InitializeComponent();
            dataCollection = new GetData();
            UtilComponent.errorDialog = new ErrorDialog();
            UtilComponent.successDialog = new SuccessDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           bool result =  dataCollection.CheckLogin(userName.Text, passWord.Password.ToString()); 
           if ( result == true)
            {
                Function fncscreen = new Function();
                NavigationService.Navigate(fncscreen);
            }
           else
            {
                UtilComponent.errorDialog.getErrorMsg("Wrong username/password ");
                UtilComponent.errorDialog.ShowDialog();
            }
        }
          
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow.backBtn.Visibility = Visibility.Hidden;
            MainWindow.logOutBtn.Visibility = Visibility.Hidden;
        }
    }
}
