﻿using POS.Database;
using POS.Model;
using POS.Utils;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;


namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for BillingDetail.xaml
    /// </summary>
    public partial class BillingDetail : Page
    {

        ObservableCollection<Database.Bill> billingList;
        GetData dataCollection;
        BindingHandler bindingHandler;
        public BillingDetail()
        {
            InitializeComponent();
            billingList = new ObservableCollection<Database.Bill>();
            dataCollection = new GetData();
            bindingHandler = new BindingHandler();
        }
        
        /// <summary>
        /// Get Billing List by filter option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultBtn_Clicked(object sender, RoutedEventArgs e)
        {
            if(DatePicker.Text != "")
            {
                DateTime Date = Convert.ToDateTime(DatePicker.Text);
                // Filter by Date
              if (chbDate.IsChecked == true)
              {
                  billingList.Clear();
                  billingList = dataCollection.GetBillingListByDate(Date);
                    if(billingList.Count > 0)
                    {
                        BillingListView.ItemsSource = billingList;
                        bindingHandler.OnPropertyChanged();
                    }                  
              }
              // Filter by Month
              else
              {
                billingList.Clear();
                int month = Date.Month;
                billingList = dataCollection.GetBillingListByMonth(month);
                    if (billingList.Count > 0)
                    {
                        BillingListView.ItemsSource = billingList;
                        bindingHandler.OnPropertyChanged();
                    }
              }
            }
        }

        /// <summary>
        /// Enable and Disable checkbox
        /// </summary>
        private void chbDate_Clicked(object sender, RoutedEventArgs e)
        {
            if (chbMonth.IsEnabled == true)
            {
                chbMonth.IsEnabled = false;
                return;
            }
            chbMonth.IsEnabled = true;
        }

        /// <summary>
        /// Enable and Disable checkbox
        /// </summary>
        private void chbMonth_clicked(object sender, RoutedEventArgs e)
        {
            if (chbDate.IsEnabled == true)
            {
                chbDate.IsEnabled = false;
                return;
            }
            chbDate.IsEnabled = true;
        }
    }
}
