﻿
using System.Windows;


namespace POS.Screen.Dialog
{
    /// <summary>
    /// Interaction logic for SuccessDialog.xaml
    /// </summary>
    public partial class SuccessDialog : Window
    {
        public SuccessDialog()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        private void SuccessBtn_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void SuccessDialog_Load(object sender, RoutedEventArgs e)
        {
       
        }

        public void GetSuccessMsg(string Msg)
        {
            txtSuccess.Text = Msg;
        }
    }
}
