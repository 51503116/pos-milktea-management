﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace POS.Screen.Dialog
{
    /// <summary>
    /// Interaction logic for BillingDialog.xaml
    /// </summary>
    public partial class BillingDialog : Page
    {
        public BillingDialog(string totalBill)
        {
            InitializeComponent();
            txtTotalBill.Text = totalBill;
        }

        private void BillingDialog_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        private void txtRecieve_Changed(object sender, TextChangedEventArgs e)
        {
            txtExchange.Text = (Int32.Parse(txtReceive.Text) - Int32.Parse(txtTotalBill.Text)) > 0 ? 
                                (Int32.Parse(txtReceive.Text) - Int32.Parse(txtTotalBill.Text)).ToString() : "0"; 
        }
    }
}
