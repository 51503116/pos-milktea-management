﻿
using System.Windows;


namespace POS.Screen.Dialog
{
    /// <summary>
    /// Interaction logic for LogoutDialog.xaml
    /// </summary>
    public partial class LogoutDialog : Window
    {
        public LogoutDialog()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        private void YesBtn_Click(object sender, RoutedEventArgs e)
        {               
            this.DialogResult = true;
            Close();
        }

        private void NoBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
