﻿using System.Windows;


namespace POS.Screen.Dialog
{
    /// <summary>
    /// Interaction logic for ErrorDialog.xaml
    /// </summary>
    public partial class ErrorDialog : Window
    {
        public ErrorDialog()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }


        private void ErrorBtn_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }


        public void getErrorMsg(string Msg)
        {
            txtError.Text = Msg;
        }
    }
}
