﻿using POS.Database;
using POS.Model;
using POS.Utils;
using System;
using System.Windows;

namespace POS.Screen.Dialog
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        DashboardDialog dashBoardDialog;
        BillingDialog billDialog;
        ItemAmountDialog amountDialog;
        ModifiedItemDialog itemDialog;
        SuccessDialog successDialog;
        HandleData handleData;
        int dialogID;
        public string itemAmount,year;
        Item modelItem;
        GetData dataCollection;

        /// <param name="value">Text value in previous screen</param>
        public CustomDialog(int screenID , string value, object obj)
        {
            InitializeComponent();
            dialogID = screenID;
            modelItem = (Item)obj;
            itemDialog = new ModifiedItemDialog(obj);
            billDialog = new BillingDialog(value);
            dashBoardDialog = new DashboardDialog();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitComponent();
            // Call dialog by Dialog Id
            switch (dialogID)
            {
                case Utils.UtilComponent.Modified_Item_Id:
                    ModifiedItemDialog(obj);
                    break;
                default:
                    HandleDialog(value); // Billing and Amount Dialog
                    break;
            }       
        }

        private void InitComponent()
        {
            handleData = new HandleData();
            dataCollection = new GetData();
            successDialog = new SuccessDialog();
            amountDialog = new ItemAmountDialog();
        }

        /// <summary>
        /// Call Dialog ( Billing and Amount )
        /// </summary>
        /// <param name="value"></param>
        private void HandleDialog(string value)
        {
            switch (dialogID)
            {
                case Utils.UtilComponent.BillingDialog_Id:                 
                    dialogMainFrame.NavigationService.Navigate(billDialog);
                    break;
                case Utils.UtilComponent.AmmountDialog_Id:
                    dialogMainFrame.NavigationService.Navigate(amountDialog);
                    break;
                case Utils.UtilComponent.DashboardDialog_Id:
                    dialogMainFrame.NavigationService.Navigate(dashBoardDialog);
                    break;
            }
        }
        /// <summary>
        /// Call Dialog ( Modified Item )
        /// </summary>
        private void ModifiedItemDialog(object obj)
        { 
            dialogMainFrame.NavigationService.Navigate(itemDialog);
        }

        /// <summary>
        /// Click Ok Btn
        /// </summary>
        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {          
            switch (dialogID)
            {             
                case Utils.UtilComponent.BillingDialog_Id :
                    try
                    {
                        DateTime currenDate = DateTime.Now;
                        Database.Bill bill = new Database.Bill();
                        bill.idAccount =  UtilComponent.user.UserName1;
                        bill.isAvaiable = 1;
                        bill.totalPrice = double.Parse(billDialog.txtTotalBill.Text);
                        bill.DateTime = currenDate;
                        handleData.InsertBill(bill);
                    } catch (Exception ex)
                    {
                        throw ex;
                    }                 
                    break;
                case Utils.UtilComponent.AmmountDialog_Id:
                    itemAmount = amountDialog.txtAmount.Text;
                    break;
                case Utils.UtilComponent.Modified_Item_Id:
                    try
                    {
                        Database.ItemDetail dbItem = new Database.ItemDetail();
                        dbItem.id = itemDialog.txtItemId.Text;
                        dbItem.name = itemDialog.txtItemName.Text;
                        dbItem.idCategory = Int32.Parse(itemDialog.txtCat.Text);
                        if (itemDialog.chbSizeL.IsChecked == true)
                        {
                            handleData.ModifyItem(dbItem, "L", itemDialog.txtPriceL.Text);
                        }
                        if (itemDialog.chbSizeM.IsChecked == true)
                        {
                            handleData.ModifyItem(dbItem, "M", itemDialog.txtPriceM.Text);
                        }
                    }
                    catch ( Exception ex)
                    {
                        throw ex;                    
                    }
                    break;
                case Utils.UtilComponent.DashboardDialog_Id:
                    year = dashBoardDialog.txtYear.Text;
                    break;
            }
            this.DialogResult = true;
            Close();
        }

        private void btnDialogCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Close();
        }
    }
}
