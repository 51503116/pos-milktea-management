﻿using POS.Database;
using POS.Model;
using POS.Utils;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows.Controls;


namespace POS.Screen.Dialog
{
    /// <summary>
    /// Interaction logic for ModifiedItemDialog.xaml
    /// </summary>
    public partial class ModifiedItemDialog : Page
    {
        Item item;
        ObservableCollection<Category> catList;
        GetData dataCollection;
        BindingHandler bindingHandler;
        Category cat;
        public ModifiedItemDialog(object obj)
        {
            InitializeComponent();
            cat = new Category();
            catList = new ObservableCollection<Category>();
            bindingHandler = new BindingHandler();
            dataCollection = new GetData();

            // Get category list
            catList = dataCollection.GetCategoryList();
            cmbCategory.ItemsSource = catList;

            if (obj != null ) // Update or Delete item
            {
                MappingData(obj);
            }
        }

        /// <summary>
        /// Mapping Data of Selected Item
        /// </summary>
        private void MappingData(object obj)
        {       
           item = (Item)obj;        
           txtItemId.Text = item.Id;
           txtItemName.Text = item.Name;
           switch (item.Size)
            {
                case "M":
                    txtPriceM.Text = item.Price.ToString();
                    txtPriceL.IsEnabled = false;
                    chbSizeL.IsChecked = false;
                    chbSizeL.IsEnabled = false;
                    break;
                default:
                    txtPriceL.Text = item.Price.ToString();
                    txtPriceM.IsEnabled = false;
                    chbSizeM.IsChecked = false;
                    chbSizeM.IsEnabled = false;
                    break;
            }
            cat = (Category)cmbCategory.SelectedItem;
            txtCat.Text = cat.Id;
            bindingHandler.OnPropertyChanged();
        }
       

        /// <summary>
        /// Status of txtPrice by checkBox ( Enable or Disable )
        private void chbSizeM_Clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (chbSizeM.IsChecked == false)
            {
                txtPriceM.IsEnabled = false;
            }else
            {
                txtPriceM.IsEnabled = true;
            }
        }


        /// <summary>
        /// Status of txtPrice by checkBox ( Enable or Disable )
        private void chbSizeL_clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (chbSizeL.IsChecked == false)
            {
                txtPriceL.IsEnabled = false;
            }else
            {
                txtPriceL.IsEnabled = true;
            }

        }

        /// <summary>
        /// Validate Type of Text 
        /// </summary>
        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        /// <summary>
        /// When change combobox 
        /// </summary>
        private void cmbCategory_SelectedChange(object sender, SelectionChangedEventArgs e)
        {
            cat = (Category)cmbCategory.SelectedItem;
            txtCat.Text = cat.Id;
        }
    }
}
