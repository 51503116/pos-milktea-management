﻿using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using POS.Model;
using System.Linq;
using POS.Utils;
using System.Collections.ObjectModel;
using POS.Database;
using POS.Screen.Dialog;
using System;

namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for Order.xaml
    /// </summary>
    public partial class Order : Page
    {
        int pageIndex = 0, numPerPage = 6;
        List<object> myList, itemTempList;
        ObservableCollection<Item> menuList;
        BindingHandler bindingHandler ;
        GetData dataCollection;

        public Order()
        {
            InitializeComponent();
            Init();
            MainWindow.backBtn.Visibility = Visibility.Visible;
        }

        private void Init()
        {
            myList = new List<object>();
            itemTempList = new List<object>();
            menuList = new ObservableCollection<Item>();
            bindingHandler = new BindingHandler();
            dataCollection = new GetData(); 
        }

        private void Order_Loaded(object sender, RoutedEventArgs e)
        {
            itemTempList = dataCollection.GetItemMenu(1);
            myList = itemTempList;
            Navigate(pageIndex, MenuContext, myList, numPerPage);
        }

        /// <summary>
        /// Event Previous Button Click of Pageignator
        /// </summary>
        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            btnNext.Visibility = Visibility.Visible;
            pageIndex--;
            pageIndex = pageIndex < 0 ? 0 : pageIndex;
            Navigate(pageIndex, MenuContext, myList, numPerPage);
        }

        /// <summary>
        /// Event Next Button Click of Pageignator
        /// </summary>
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            btnPrev.Visibility = Visibility.Visible;
            pageIndex++;
            DisplayBtn(btnNext);
            Navigate(pageIndex, MenuContext, myList, numPerPage);
        }

        /// <summary>
        /// Navigate to next pageignator
        /// </summary>
        public void Navigate(int pageIndex, Grid dataGrid, List<object> myList, int numberOfRecPerPage)
        {
            dataGrid.Children.Clear();
            LayoutConfig layoutConfig = new LayoutConfig();
            int i = 1, column = 0, row = 0;
            foreach (Item itemMenu in myList.Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage))
            {
                column = column > 1 ? 0 : column;
                Button btnItem = new Button();
                btnItem.Content = itemMenu.Category == 1 ?  ( itemMenu.Name +" - "+ itemMenu.Size ) : itemMenu.Name;
                // config button layout
                layoutConfig.MenuItemLayout(btnItem, row, column, 150, 100);
                // if 2 items has in one row => apply new row for next item
                row = (i % 2) == 0 ? (row + 1) : row;
                column += 1;
                i++;
                dataGrid.Children.Add(btnItem);
                btnItem.Click += BtnItem_Click;
                btnItem.Tag = itemMenu;
            }
        }

        /// <summary>
        /// Enable or Disable Item
        /// </summary>
        private void DisplayBtn(Button btn)
        {   
           int totalCurrentItem = (pageIndex + 1) * numPerPage;
           // Last page not enough item to navigate next and last page is fit with all item.
           if (myList.Skip((pageIndex) * numPerPage).Take(numPerPage).Count() < numPerPage || totalCurrentItem == myList.Count())
             {
               btn.Visibility = Visibility.Hidden;
             }      
        }

        /// <summary>
        /// Click Event on Menu Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnItem_Click(object sender, RoutedEventArgs e)
        {
            SelectedMenuItem.ItemsSource = menuList;
            Button btn = new Button() ;
            btn = (Button)sender;
            Item tempItemMenu = btn.Tag as Item;
            Item itemMenu = new Item(tempItemMenu.Id, tempItemMenu.Name, tempItemMenu.Size, tempItemMenu.Category, tempItemMenu.Price);
            if (itemMenu.Category == 3)
            {
                AddTopping(itemMenu); 
                return;
            }
            menuList.Add(itemMenu);   
            SelectedMenuItem.Items.Refresh();
            bindingHandler.OnPropertyChanged();
        }

        /// <summary>
        /// itemTopping : selected topping
        /// item : drink in selected menu
        /// </summary>
        private void AddTopping(Item itemTopping)
        {
            Item item  = (Item) SelectedMenuItem.SelectedItem;
            int drinkIndex = SelectedMenuItem.SelectedIndex;  
            if (SelectedMenuItem.SelectedItem != null && item.Category == 1)
            {              
                itemTopping.Name = "   " + itemTopping.Name;
                menuList.Insert(drinkIndex + 1, itemTopping);              
                SelectedMenuItem.Items.Refresh();
                bindingHandler.OnPropertyChanged();
            }
        }
        /// <summary>
        /// Delete Selected Menu Item
        /// </summary>
        private void DelBtn_Click(object sender, RoutedEventArgs e)
        {
           if( SelectedMenuItem.SelectedItem !=null )
            {
                Item itemMenu = (Item) SelectedMenuItem.SelectedItems[0];
                menuList.Remove(itemMenu);
                SelectedMenuItem.Items.Refresh();
                bindingHandler.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Add more food/drink
        /// </summary>
        private void PlusItemBtn_Click(object sender, RoutedEventArgs e)
        {
            CalculateItem(1);
        }

        /// <summary>
        /// Minus amount of food/drink
        /// </summary>
        private void MinusItemBtn_Click(object sender, RoutedEventArgs e)
        {
            CalculateItem(2);
        }

        /// <summary>
        /// Calculate item amount
        /// </summary>
        /// <param name="mode"> 1 : Plus , 2 : Minus</param>
        private void CalculateItem(int mode)
        {
            if (SelectedMenuItem.SelectedItem != null)
            {
                Item item = (Item)SelectedMenuItem.SelectedItems[0];
                switch (mode)
                {
                    case 1:
                        item.Amount = item.Amount + 1;
                        break;
                    case 2:
                        item.Amount = item.Amount - 1;
                        break;
                } 
                if(item.Amount < 1)
                {
                    menuList.Remove(item);
                }
                SelectedMenuItem.Items.Refresh();
                bindingHandler.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Compute Bill
        /// </summary>
        private void BillingBtn_Click(object sender, RoutedEventArgs e)
        {
            int totalBill = 0;
            foreach (var item in menuList)
            {
                totalBill += item.Amount * item.Price;
            }
            var billingDialog = new CustomDialog(UtilComponent.BillingDialog_Id,totalBill+"",null);
            bool? result = billingDialog.ShowDialog();
            if (result == true)
            {
                // Call success Dialog
                UtilComponent.successDialog.GetSuccessMsg("Done");
                UtilComponent.successDialog.ShowDialog();
                // Clear selected item list => prepare for new order
                menuList.Clear();
                SelectedMenuItem.Items.Refresh();
                bindingHandler.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Change item Amount
        /// </summary>
        private void AmountBtn_Click(object sender, RoutedEventArgs e)
        {        
            var amountDialog = new CustomDialog(UtilComponent.AmmountDialog_Id,"",null);
            bool? result = amountDialog.ShowDialog();
            if (result == true)
            {
                if (SelectedMenuItem.SelectedItem != null)
                {
                    Item item = (Item)SelectedMenuItem.SelectedItems[0];
                    item.Amount = Int32.Parse(amountDialog.itemAmount);
                    SelectedMenuItem.Items.Refresh();
                    bindingHandler.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Topping List 
        /// </summary>
        private void ToppingBtn_Click(object sender, RoutedEventArgs e)
        {
            myList = dataCollection.GetItemMenu(3);
            Navigate(pageIndex, MenuContext, myList, numPerPage);
        }

        /// <summary>
        /// Food List
        /// </summary>
        private void FoodBtn_Click(object sender, RoutedEventArgs e)
        {
            myList = dataCollection.GetItemMenu(2);
            Navigate(pageIndex, MenuContext, myList, numPerPage);
        }

        /// <summary>
        /// Drink List
        /// </summary>
        private void DrinkBtn_Click(object sender, RoutedEventArgs e)
        {
            myList = dataCollection.GetItemMenu(1);
            Navigate(pageIndex, MenuContext, myList, numPerPage);
        }
    }
}
