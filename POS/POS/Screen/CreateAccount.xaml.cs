﻿using POS.Database;
using POS.Screen.Dialog;
using POS.Utils;
using System.Windows;
using System.Windows.Controls;


namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for CreateAccount.xaml
    /// </summary>
    public partial class CreateAccount : Page
    {
        int isAdmin;
        HandleData dataHandle;
        public CreateAccount()
        {
            InitializeComponent();
            dataHandle = new HandleData();
        }

        /// <summary>
        /// Enable isAdmin
        /// </summary>
        private void chbIsAdmin_Click(object sender, RoutedEventArgs e)
        {
            if (chbIsAdmin.IsChecked == true)
            {
                isAdmin = 1;
            }
            else
            {
                isAdmin = 0;
            }
        }

        /// <summary>
        /// Compare password and confirm password
        /// </summary>
        private bool CheckPassword()
        {
            if (pwdBoxPassword.Password.ToString().Equals(pwdBoxConfirmPwd.Password.ToString()))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Create Account
        /// </summary>
        private void CreateAccountBtn_Click(object sender, RoutedEventArgs e)
        {
          
            if (CheckPassword() == false)
            {
                UtilComponent.errorDialog.getErrorMsg("Password and Confirm Password not match");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }
            string pwd = UtilComponent.Base64Encode(pwdBoxPassword.Password.ToString());
            bool result = dataHandle.CreateAccount(txtUserName.Text, txtDisplayName.Text,pwd, isAdmin);
            if(result == true)
            {
                UtilComponent.successDialog.GetSuccessMsg("Create Success");
                UtilComponent.successDialog.ShowDialog();
                this.NavigationService.GoBack();
            }else
            {
                UtilComponent.errorDialog.getErrorMsg("User Name is existed - Change your User Name");
                UtilComponent.errorDialog.ShowDialog();
            }

        }
    }
}
