﻿using POS.Database;
using POS.Model;
using POS.Screen.Dialog;
using POS.Utils;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows;


namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        ObservableCollection<Item> menuList;
        GetData dataCollection;
        BindingHandler bindingHandler;
        HandleData hanleData;
        int foodType;
        public Menu()
        {
            InitializeComponent();
            MainWindow.backBtn.Visibility = Visibility.Visible;
            dataCollection = new GetData();
            bindingHandler = new BindingHandler();
            menuList = new ObservableCollection<Item>();
           
        }

        private void Menu_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            menuList = dataCollection.GetItemMenu2(1);
            foodType = 1;
            MenuItemList.ItemsSource = menuList;
            bindingHandler.OnPropertyChanged();
        }

        /// <summary>
        /// Show Drink List
        /// </summary>
        private void DrinkBtn_Clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            menuList.Clear();
            menuList = dataCollection.GetItemMenu2(1);
            foodType = 1;
            bindingHandler.OnPropertyChanged();
        }

        /// <summary>
        /// Show Food List
        /// </summary>
        private void FoodBtn_Clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            menuList.Clear();
            menuList = dataCollection.GetItemMenu2(2);
            foodType = 2;
            bindingHandler.OnPropertyChanged();
        }

        /// <summary>
        /// Show Topping List
        /// </summary>
        private void ToppingBtn_Clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            menuList.Clear();
            menuList = dataCollection.GetItemMenu2(3);
            foodType = 3;
            bindingHandler.OnPropertyChanged();
        }

        /// <summary>
        /// Create new Item
        /// </summary>
        private void AddBtn_Clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            // check isAdmin
            if (Utils.UtilComponent.user.checkIsAdmin() == false)
            {
                UtilComponent.errorDialog.getErrorMsg("Do not allow to create !");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }

            bool? result = CallModifiedItemDialog(null);
            if (result == true)
            {
                UtilComponent.successDialog.GetSuccessMsg("Create success");
                UtilComponent.successDialog.ShowDialog();
                menuList = dataCollection.GetItemMenu2(foodType);
                bindingHandler.OnPropertyChanged();
            }        
        }
        /// <summary>
        /// Update Selected Item
        /// </summary>
        private void UpdateBtn_Clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            // check isAdmin
            if (Utils.UtilComponent.user.checkIsAdmin() == false)
            {
                UtilComponent.errorDialog.getErrorMsg("Do not allow to update !");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }

            if (MenuItemList.SelectedItem != null)
            {
                Item item;
                item = (Item) MenuItemList.SelectedItem;
                bool? result = CallModifiedItemDialog(item);
                if (result == true)
                {
                    UtilComponent.successDialog.GetSuccessMsg("Update Success");
                    UtilComponent.successDialog.ShowDialog();
                    MenuItemList.Items.Refresh();
                    bindingHandler.OnPropertyChanged();
                }
                else
                {
                    UtilComponent.errorDialog.getErrorMsg("Can not update !");
                    UtilComponent.errorDialog.ShowDialog();                 
                }
            }
        }

        /// <summary>
        /// Disable Item - Delete Item
        /// </summary>
        private void DelBtn_Clicked(object sender, System.Windows.RoutedEventArgs e)
        {
            // check isAdmin
            if (Utils.UtilComponent.user.checkIsAdmin() == false)
            {
                UtilComponent.errorDialog.getErrorMsg("Do not allow to delete !");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }

            if (MenuItemList.SelectedItem != null)
            {
                Item item;
                item = (Item) MenuItemList.SelectedItem;
                hanleData = new HandleData();
                hanleData.DeleteItem(item);
                menuList.Remove(item);
                bindingHandler.OnPropertyChanged();
                UtilComponent.successDialog.GetSuccessMsg("Delete Success");
                UtilComponent.successDialog.ShowDialog();
            }
        }

        /// <summary>
        /// Show Dialog 
        /// </summary>
        private bool? CallModifiedItemDialog(object obj)
        {
            var modifiedItemDialog = new CustomDialog(UtilComponent.Modified_Item_Id, "", obj);
            bool? result = modifiedItemDialog.ShowDialog();
            return result;
        }
    }
}
