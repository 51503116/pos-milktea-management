﻿using POS.Screen.Dialog;
using POS.Utils;
using System.Windows;
using System.Windows.Controls;


namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for Account.xaml
    /// </summary>
    public partial class Account : Page
    {
        public Account()
        {
            InitializeComponent();
            MainWindow.backBtn.Visibility = Visibility.Visible;
        }

        private void ChangePswBtn_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword changePswPg = new ChangePassword();
            this.NavigationService.Navigate(changePswPg);
        }

        private void CreateAccountBtn_Click(object sender, RoutedEventArgs e)
        {
            if (UtilComponent.user.checkIsAdmin() == false)
            {
                UtilComponent.errorDialog.getErrorMsg("Authentication Fail !");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }
            CreateAccount createAccPg = new CreateAccount();
            NavigationService.Navigate(createAccPg);
        }
    }
}
