﻿
using System.Windows;
using System.Windows.Controls;


namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for Function.xaml
    /// </summary>
    public partial class Function : Page
    {
        public Function()
        {
            InitializeComponent();
            MainWindow.logOutBtn.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void OderBtn_Click(object sender, RoutedEventArgs e)
        {
            Order oderPg = new Order();
            this.NavigationService.Navigate(oderPg);
        }

        private void BillingBtn_Click(object sender, RoutedEventArgs e)
        {    
            Billing billPg = new Billing();
            this.NavigationService.Navigate(billPg);
        }

        /// <summary>
        /// Invisible Back Button
        /// </summary>
        private void FunctionPage_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow.backBtn.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Update Menu Information
        /// </summary>
        private void BtnMenu_Clicked(object sender, RoutedEventArgs e)
        {
            Menu menuScr = new Menu();
            this.NavigationService.Navigate(menuScr);
        }

        private void AccountBtn_Click(object sender, RoutedEventArgs e)
        {
            Account accountScr = new Account();
            this.NavigationService.Navigate(accountScr);
        }
    }
}
