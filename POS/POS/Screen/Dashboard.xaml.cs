﻿using POS.Database;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;

namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Page
    {
        private string _year;
        GetData dataCollection;
        public Dashboard(string year)
        {            
            InitializeComponent();
            _year = year;
            dataCollection = new GetData();
        }

        private void Dashboard_Loaded(object sender, RoutedEventArgs e)
        {
            KeyValuePair<string, double>[] arr = new KeyValuePair<string, double>[12];

            for ( int i = 1; i < 13; i++)
            {
                double billbyMonth = dataCollection.GetBill_Month_Year(i, _year);
                var valuePerMonth = new KeyValuePair<string, double>(""+i, billbyMonth);
                arr[i-1] = valuePerMonth;
            }

            ((ColumnSeries)mcChart.Series[0]).ItemsSource = arr;
        }
    }
}
