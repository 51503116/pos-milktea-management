﻿using POS.Screen.Dialog;
using POS.Utils;
using System.Windows;
using System.Windows.Controls;


namespace POS.Screen
{
    /// <summary>
    /// Interaction logic for Billing.xaml
    /// </summary>
    public partial class Billing : Page
    {
        public Billing()
        {
            InitializeComponent();
            MainWindow.backBtn.Visibility = Visibility.Visible;
        }

        private void DashboardBtn_Click(object sender, RoutedEventArgs e)
        {
            // check isAdmin
            if (Utils.UtilComponent.user.checkIsAdmin() == false)
            {
                UtilComponent.errorDialog.getErrorMsg("Authientication Fail !");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }
            var dashBoardDialog = new CustomDialog(UtilComponent.DashboardDialog_Id, "", null);
            bool? result = dashBoardDialog.ShowDialog();
            if (result == true)
            {
                string year = dashBoardDialog.year;
                Dashboard dashboardPg = new Dashboard(year);
                this.NavigationService.Navigate(dashboardPg);
            }
        }

        private void BillBtn_Click(object sender, RoutedEventArgs e)
        {
            BillingDetail billingDetailPg = new BillingDetail();
            this.NavigationService.Navigate(billingDetailPg);
        }

    }
}
