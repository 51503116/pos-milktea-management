﻿using POS.Database;
using POS.Utils;
using System.Windows;
using System.Windows.Controls;


namespace POS.Screen.Dialog
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Page
    {
        GetData dataCollection;
        HandleData dataHandle; 
        public ChangePassword()
        {
            InitializeComponent();
            dataCollection = new GetData();
            dataHandle = new HandleData();
        }

        /// <summary>
        /// Change Password Btn
        /// </summary>
        private void ChangePwdBtn_Click(object sender, RoutedEventArgs e)
        {
            string currentPwd = txtCurrentpsw.Password.ToString();
            string newPwd = txtNewpws.Password.ToString();
            string confirmPwd = txtConfirmpsw.Password.ToString();
            // Check current password
            if(currentPwd !=null && CheckCurrentPwd(currentPwd) == false)
            {
                UtilComponent.errorDialog.getErrorMsg("Wrong Current Password");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }
            // Check new pwd and confirm pwd
            if(newPwd != null && confirmPwd != null && !newPwd.Equals(confirmPwd))
            {
                UtilComponent.errorDialog.getErrorMsg("Check your new/confirm password again");
                UtilComponent.errorDialog.ShowDialog();
                return;
            }
            if(newPwd != "" && confirmPwd != "")
            {
                // If all are correct => update pwd
                string encapPwd = Base64Encode(newPwd);
                bool result = dataHandle.ChangePassword(encapPwd);
                if (result == true)
                {
                    UtilComponent.successDialog.GetSuccessMsg("Your password is changed !");
                    UtilComponent.successDialog.ShowDialog();
                    NavigationService.GoBack();
                }
                else
                {
                    UtilComponent.errorDialog.getErrorMsg("Can't change your password");
                    UtilComponent.errorDialog.ShowDialog();
                }
            }
            // if current pwd is correct but not enter new / confirm pwd
            else
            {
                UtilComponent.errorDialog.getErrorMsg("Please enter your new / confirm password");
                UtilComponent.errorDialog.ShowDialog();
            }         
        }

        /// <summary>
        /// Check current pwd of logged user
        /// </summary>
        private bool CheckCurrentPwd( string pwd )
        {
            string currentPwd = Base64Encode(pwd);
            bool result = dataCollection.CheckCurrentPwd(currentPwd);
            return result;
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
