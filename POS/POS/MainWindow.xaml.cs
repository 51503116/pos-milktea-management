﻿
using System.Windows;
using POS.Screen;
using System.Windows.Controls;
using System.Windows.Threading;
using System;
using POS.Utils;
using POS.Screen.Dialog;

namespace POS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Button backBtn;
        public static Button logOutBtn;
        Login loginScreen;
        public MainWindow()
        {
            InitializeComponent();
            backBtn = BackBtn;
            logOutBtn = LogOutBtn;
            DispatcherTimer timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                this.dateText.Text = DateTime.Now.ToString("HH:mm");
            }, this.Dispatcher);
            Login lgScreen = new Login();
            mainFrame.NavigationService.Navigate(lgScreen);
        }

        private void Grid_AccessKeyPressed(object sender, System.Windows.Input.AccessKeyPressedEventArgs e)
        {

        }

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.NavigationService.GoBack();
        }

        private void LogOutBtn_Click(object sender, RoutedEventArgs e)
        {
            var logOutDialog = new LogoutDialog();
            bool? result = logOutDialog.ShowDialog();
            
            if( result == true)
            {
                UtilComponent.user = null;
                loginScreen = new Login();
                mainFrame.NavigationService.Navigate(loginScreen);
            }
         
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
