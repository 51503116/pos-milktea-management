﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Model
{
    class Item
    {
        private string id;
        private string name;
        private string size;
        private int category;
        private int price;
        private int amount;

        public Item() { }
        public Item(string id, string name,string size, int category, int price)
        {
            this.id = id;
            this.name = name;
            this.Size = size;
            this.category = category;
            this.price = price;
            this.Amount = 1;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int Category
        {
            get
            {
                return category;
            }

            set
            {
                category = value;
            }
        }

        public int Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }

        public int Amount
        {
            get
            {
                return amount;
            }

            set
            {
                amount = value;
            }
        }

        public string Size
        {
            get
            {
                return size;
            }

            set
            {
                size = value;
            }
        }
    }
}
