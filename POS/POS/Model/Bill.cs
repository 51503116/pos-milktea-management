﻿using System;

namespace POS.Model
{
    class Bill
    {
        private int idBill, isAvailable, totalPrice;
        private string idAccount;
        private DateTime dateTime;

        public Bill()
        {
        }

        public Bill(int idBill, string idAccount, DateTime dateTime, int totalPrice)
        {
            this.idBill = idBill;
            //this.IsAvailable = isAvailable;
            this.totalPrice = totalPrice;
            this.idAccount = idAccount;
            this.dateTime = dateTime;
        }

        public int IdBill
        {
            get
            {
                return idBill;
            }
            set
            {
                idBill = value;
            }
        }

        public int IsAvailable
        {
            get
            {
                return isAvailable;
            }

            set
            {
                isAvailable = value;
            }
        }

        public int TotalPrice
        {
            get
            {
                return totalPrice;
            }

            set
            {
                totalPrice = value;
            }
        }

        public string IdAccount
        {
            get
            {
                return idAccount;
            }

            set
            {
                idAccount = value;
            }
        }

        public DateTime DateTime
        {
            get
            {
                return dateTime;
            }

            set
            {
                dateTime = value;
            }
        }

       
    }
}
