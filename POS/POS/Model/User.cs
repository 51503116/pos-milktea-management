﻿
namespace POS.Model
{
    class User
    {
        private string UserName;
        private string DisplayName;
        private int isAdmin;

        public User(string userName, string displayName, int isAdmin)
        {
            UserName = userName;
            DisplayName1 = displayName;
            this.isAdmin = isAdmin;
        }

        public string UserName1
        {
            get
            {
                return UserName;
            }

            set
            {
                UserName = value;
            }
        }

        public string DisplayName1
        {
            get
            {
                return DisplayName;
            }

            set
            {
                DisplayName = value;
            }
        }

        public bool checkIsAdmin()
        {
            if(this.isAdmin == 1)
            return true;
            return false;
        }
      

      
    }
}
