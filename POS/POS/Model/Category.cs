﻿

namespace POS.Model
{
    class Category
    {
        private string id;
        private string name;
        public Category()
        {

        }
        public Category(string id, string name)
        {
            this.id = id;
            this.name = name;
        }
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
    }
}
