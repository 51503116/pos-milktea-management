﻿
using POS.Model;
using POS.Screen.Dialog;

namespace POS.Utils
{
    class UtilComponent
    {
        public const int BillingDialog_Id = 1; // Dialog Billing
        public const int AmmountDialog_Id = 2; // Dialog Amount
        public const int Modified_Item_Id = 3; // Dialog Handle Item Information ( Create , Update , Delete )
        public const int DashboardDialog_Id = 6; // Dialog Dashboard        
        public const string CONNECTTION_STRING = "Data Source=DESKTOP-1R3CVQV;Initial Catalog=MilkTeaManagement;Integrated Security=true";
        //Logged user in application
        public static User user;
        public static ErrorDialog errorDialog;
        public static SuccessDialog successDialog;

        // encrypt pwd
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

    }
}
