﻿
using System.Windows.Controls;

namespace POS.Utils
{
    class LayoutConfig
    {
        public LayoutConfig() { }
        public void MenuItemLayout(Button item, int row, int column, int width, int height)
        {
            item.SetValue(Grid.ColumnProperty, column);
            item.SetValue(Grid.RowProperty, row);
            item.Height = height;
            item.Width = width;
        }
    }
}
