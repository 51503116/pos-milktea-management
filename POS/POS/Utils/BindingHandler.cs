﻿
using System.ComponentModel;

using System.Runtime.CompilerServices;

namespace POS.Utils
{
    class BindingHandler : INotifyPropertyChanged
    {
        public BindingHandler()
        {

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
