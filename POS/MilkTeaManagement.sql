CREATE DATABASE MilkTeaManagement
GO

USE MilkTeaManagement
GO

-- Food
-- Table
-- FoodCategory
-- Account
-- Bill
-- BillInfo


CREATE TABLE Account
(
	UserName NVARCHAR(100) PRIMARY KEY,	
	DisplayName NVARCHAR(100) NOT NULL DEFAULT N'AdminPos',
	PassWord NVARCHAR(1000) NOT NULL DEFAULT 0,
	Phone VARCHAR(11)NOT NULL,
	isAdmin INT NOT NULL  DEFAULT 0 ,-- 1: admin && 0: staff
	isAvaiable INT NOT NULL  DEFAULT 1 ,-- 1: Active && 0: Inactive
)
GO

CREATE TABLE ItemCategory
(
	id INT IDENTITY PRIMARY KEY ,
	name NVARCHAR(100) NOT NULL DEFAULT N'Unknow Category',
	isAvaiable INT NOT NULL  DEFAULT 1 ,-- 1: Active && 0: Inactive
)
GO

CREATE TABLE ItemDetail
(
	id NVARCHAR(100) PRIMARY KEY,
	name NVARCHAR(100) NOT NULL DEFAULT N'Unknow Item',
	idCategory INT NOT NULL,
	isAvaiable INT NOT NULL  DEFAULT 1 ,-- 1: Active && 0: Inactive
	FOREIGN KEY (idCategory) REFERENCES dbo.ItemCategory(id)
)
GO

CREATE TABLE ItemPrice
(
	id_itemprice NVARCHAR(100),
	size nvarchar(2) default ' ',
	isAvaiable INT NOT NULL  DEFAULT 1 ,-- 1: Active && 0: Inactive
	CONSTRAINT Pk_ItemPrice PRIMARY KEY (id_itemprice,size),
	FOREIGN KEY (id_itemprice) REFERENCES dbo.ItemDetail(id)
)
GO

CREATE TABLE Bill
(
	id_Bill INT IDENTITY PRIMARY KEY,
	idAccount NVARCHAR(100) NOT NULL,
	DateTime DATETIME NOT NULL DEFAULT GETDATE(),
	isAvaiable INT NOT NULL  DEFAULT 1 ,-- 1: Active && 0: Inactive
	totalPrice FLOAT,
	FOREIGN KEY (idAccount) REFERENCES dbo.Account(UserName)
)
GO
--ALTER TABLE Bill ALTER COLUMN DateTime Date; 
--GO

CREATE TABLE ItemInfor
(
	id INT IDENTITY PRIMARY KEY,
	idBill INT NOT NULL,
	idItem INT NOT NULL,
	priceItem FLOAT DEFAULT 0, --Gia ly nuoc sau khi discount (neu co)
	discount FLOAT DEFAULT 0, --Discount [0..1]
	priceItemAndToppings FLOAT DEFAULT 0, --Gia tong bao gom luon topping
	quantity INT,
	totalPrice FLOAT,
	FOREIGN KEY (idItem) REFERENCES dbo.Item(id),
	FOREIGN KEY (idBill) REFERENCES dbo.Bill(id)
)
GO

CREATE TABLE ItemTopping
(
	id INT IDENTITY PRIMARY KEY,
	idItemInfor INT NOT NULL,
	idTopping INT, -- Khoa ngoai den item
	price FLOAT, --Gia Topping
	FOREIGN KEY (idItemInfor) REFERENCES dbo.ItemInfor(id),
	FOREIGN KEY (idTopping) REFERENCES dbo.Item(id)
)
GO





--Create PROC Login
CREATE PROC USP_Login
@username NVARCHAR(100), @pass NVARCHAR(100)
AS
BEGIN
	SELECT * FROM dbo.Account WHERE UserName = @username AND PassWord = @pass AND isAvaiable = 1
END
GO

-- Create Get View Bill
CREATE PROC USP_GetViewBill
@id INT	
AS
BEGIN
	SELECT ii.idBill, ii.id as ItemInforId, f.name, f1.name as Topping, ii.quantity, ii.totalPrice 
	FROM ItemInfor ii
	Left Join ItemTopping On ii.id = ItemTopping.idItemInfor
	Left Join Item f1 on ItemTopping.idTopping = f1.id
	Left Join Item f on ii.idItem = f.id
	Where idBill = @id
END
GO



--Create proc UpdateAccount
CREATE PROC USP_UpdateAccount
@userName NVARCHAR(100), @displayName NVARCHAR(100), @password NVARCHAR(100), @newPassword NVARCHAR(100)
AS
BEGIN
	DECLARE @isRightPass INT = 0
	
	SELECT @isRightPass = COUNT(*) FROM dbo.Account WHERE USERName = @userName AND PassWord = @password
	
	IF (@isRightPass = 1)
	BEGIN
		IF (@newPassword = NULL OR @newPassword = '')
		BEGIN
			UPDATE dbo.Account SET DisplayName = @displayName WHERE UserName = @userName
		END		
		ELSE
			UPDATE dbo.Account SET DisplayName = @displayName, PassWord = @newPassword WHERE UserName = @userName
	end
END
GO

INSERT INTO dbo.Account
        ( UserName ,
          DisplayName ,
          PassWord ,
          PID ,
          Phone ,
          isAdmin ,
          isAvaiable
        )
VALUES  ( N'datcachua2' , -- UserName - nvarchar(100)
          N'Qu?c ??t' , -- DisplayName - nvarchar(100)
          N'123456' , -- PassWord - nvarchar(1000)
          '09828374' , -- PID - varchar(12)
          '0907901143' , -- Phone - varchar(11)
          0 , -- isAdmin - int
          1  -- isAvaiable - int
        )
GO

INSERT INTO dbo.Account
        ( UserName ,
          DisplayName ,
          PassWord ,
          PID ,
          Phone ,
          isAdmin ,
          isAvaiable
        )
VALUES  ( N'Dat' , -- UserName - nvarchar(100)
          N'N L Qu?c ??t' , -- DisplayName - nvarchar(100)
          N'123456' , -- PassWord - nvarchar(1000)
          '09828374' , -- PID - varchar(12)
          '0907901143' , -- Phone - varchar(11)
          1 , -- isAdmin - int
          1  -- isAvaiable - int
        )
GO

INSERT dbo.Bill
        ( idAccount ,
          DateTime ,
          isAvaiable ,
          totalPrice
        )
VALUES  ( N'dat' , -- idAccount - nvarchar(100)
          GETDATE() , -- DateTime - date
          0 , -- isAvaiable - int
          0.0  -- totalPrice - float
        )
GO

--PROC GetBillByDate
CREATE PROC USP_GetListBillByDate
@dateFrom date, @dateTo date
AS 
BEGIN
	SELECT b.id AS [Bill ID], b.totalPrice AS [T?ng ti?n], DateTime AS [Ng�y], a.DisplayName AS [Ng??i l?p]
	FROM Bill b, Account a
	WHERE Convert(date,b.DateTime) >= @dateFrom AND Convert(date,b.DateTime) <= @dateTo AND b.isAvaiable = 1
	AND b.idAccount = a.UserName
END
GO

